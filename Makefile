.PHONY: install update test clean

install:
	composer install
	make test

update:
	composer update
	composer dumpautoload
	make test

test: clean
	./vendor/bin/phpunit --testdox

clean:
	rm -rf ./test/_reports